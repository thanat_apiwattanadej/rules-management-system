<?php

require_once "lib/nusoap.php";

$client = new soapclient("http://localhost/RMS/server.php",false);

$error = $client->getError();

if($error){
	echo "<h2>Constructor error</h2><pre>".$error."</pre>";
}

if($_POST['getRules']){
	$result = $client->call("getRules");
}

if($_POST['addRule']){
	$rule_name = $_POST['rule_name'];
	$point = $_POST['point'];
	$result = $client->call("addRule",array("ruleName"=>$rule_name,"pts"=>$point));
}

if($_POST['setRule']){
	$rule_id = $_POST['rule_id'];
	$rule_name = $_POST['rule_name'];
	$point = $_POST['point'];
	$result = $client->call("setRule",array("ruleID"=>$rule_id,"ruleName"=>$rule_name,"pts"=>$point));
}

if($_POST['delRule']){
	$rule_id = $_POST['rule_id'];
	$result = $client->call("deleteRule",array("ruleID"=>$rule_id));
}

$error2 = $client->getError();

if($error2){
	echo "<h2>Error</h2><pre>".$error2."</pre>";
}
else{
	echo $result;
}
?>