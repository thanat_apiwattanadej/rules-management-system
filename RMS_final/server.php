<?php

require_once "lib/nusoap.php";

function createConnection($host,$username,$password,$dbname){
	// Create connection
	$con=mysqli_connect($host,$username,$password,$dbname);

	// Check connection
	if (mysqli_connect_errno()){
		return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function closeConnection($con){
	mysqli_close($con);
}

function getRules(){
	$con = createConnection("localhost","root","root","rewarddatabase");
	$query = "SELECT Rule_ID,Rule_Name,Point FROM rules ORDER BY rule_id";
	
	$result = mysqli_query($con,$query);
	
	closeConnection($con);
	
	$table = displayRule($result);		// Comment th width=\"150\"is line to disable displayRule
	return $table;								// Then, change th width=\"150\"e returned value to $result
}

function addRule($ruleName,$point,$curDate,$endDate){
	$con = createConnection("localhost","root","root","rewarddatabase");
	
	$query="INSERT INTO rules (Rule_Name, Point, Duration)
				 VALUES ('".$ruleName."',".$point.",".$endDate.")";
			
	$result = mysqli_query($con,$query);
	
	if (!$result){
		die('Error: ' . mysqli_error($con));
	}	 
	closeConnection($con);
	$table = getRules();
	return $table;
	//return "Add Successful";
}

function setRule($ruleID,$ruleName,$point,$curDate){
	$con = createConnection("localhost","root","root","rewarddatabase");
	
	$query="UPDATE rules
				 SET Rule_Name='".$ruleName."',Point=".$point." ".
				 "WHERE Rule_ID=".$ruleID;
			
	$result = mysqli_query($con,$query);
	
	if (!$result){
		die('Error: ' . mysqli_error($con));
	}
	 
	closeConnection($con);
	$table = getRules();
	return $table;
	//return "Update Successful";
}

function deleteRule($ruleID){	
	$con = createConnection("localhost","root","root","rewarddatabase");
	
	$query="DELETE FROM rules WHERE Rule_ID=".$ruleID;
			
	$result = mysqli_query($con,$query);
	
	if (!$result){
		die('Error: ' . mysqli_error($con));
	}
	 
	closeConnection($con);
	$table = getRules();
	return $table;
	//return "Delete Successful";	
}

function displayRule($result){
	$table = "<table border='1'>
	<tr>
	<th width=\"150\">Rule_ID</th width=\"150\">
	<th width=\"150\">Rule_Name</th width=\"150\">
	<th width=\"150\">Point</th width=\"150\">
	<th width=\"120\">Option</th width=\"150\">
	</tr>";

	while($row = mysqli_fetch_array($result))
	  {
		  $table .= "<tr id = \"" . $row['Rule_ID'] . "\">";
		  $table .= "<td>" . $row['Rule_ID'] . "</td>";
		  $table .= "<td>" . $row['Rule_Name'] . "</td>";
		  $table .= "<td>" . $row['Point'] . "</td>";
		  $table .= "<td><button type=\"button\" onclick=\"deleteRule(" . $row['Rule_ID'] . ")\">Delete</button>
						 <button type=\"button\" id=\"edit\" onclick=\"editRule(" . $row['Rule_ID'] .",'". $row['Rule_Name'] ."','". $row['Point'] ."')\">Edit</button>
					</td>";
		  $table .= "</tr><tr id = \"e" . $row['Rule_ID'] . "\"></tr>";
	  }
	$table .= "</table>";
	
	return $table;
}

// create object to deal with width=\"150\" service provider
$server = new soap_server();
$server->register("getRules");
$server->register("addRule");
$server->register("setRule");
$server->register("deleteRule");

if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$server->service($HTTP_RAW_POST_DATA);

?>