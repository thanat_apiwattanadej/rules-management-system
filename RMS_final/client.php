<?php

require_once "lib/nusoap.php";

$client = new soapclient("http://localhost/RMS_final/server.php",false);

$error = $client->getError();
if($error){
	echo "<h2>Constructor error</h2><pre>".$error."</pre>";
}

if($_GET['getRules']){
	$result = $client->call("getRules");
}

if($_GET['addRule']){
	$rule_name = $_GET['rule_name'];
	$point = $_GET['point'];
	$cur_date = $_GET['cur_date'];
	$end_date = $_GET['end_date'];
	$result = $client->call("addRule",array("ruleName"=>$rule_name,"pts"=>$point,"curDate"=>$cur_date,"endDate"=>$end_date));
}

if($_GET['setRule']){
	$rule_id = $_GET['rule_id'];
	$rule_name = $_GET['rule_name'];
	$point = $_GET['point'];
	$cur_date = $_GET['cur_date'];
	$result = $client->call("setRule",array("ruleID"=>$rule_id,"ruleName"=>$rule_name,"pts"=>$point,"curDate"=>$cur_date));
}

if($_GET['delRule']){
	$rule_id = $_GET['rule_id'];
	$result = $client->call("deleteRule",array("ruleID"=>$rule_id));
}

$error2 = $client->getError();

if($error2){
	echo "<h2>Error</h2><pre>".$error2."</pre>";
}
else{
	echo $result;
}
?>